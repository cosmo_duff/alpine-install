#!/bin/bash

set -e

DRIVE=$1
ENCRYPT=$2

if [ -z $DRIVE ]
then
	echo "Please supply a drive to perform installtion on..."
	exit 1
fi

export USE_EFI=1


if [ -z $ENCRYPT ]
then
	SYS_LABEL="system"
	SWAP_LABEL="swap"
else
	SYS_LABEL="cryptsystem"
	#SWAP_LABEL="cryptswap"
	SWAP_LABEL="swap"
fi

setup_encryption() {
	# create the encrypted luks partition
	cryptsetup -v -c serpent-xts-plain64 -s 512 --hash whirlpool --iter-time 5000 --use-random luksFormat /dev/disk/by-partlabel/cryptsystem
	
	# unlock the newly created encrypted partition
	cryptsetup open /dev/disk/by-partlabel/cryptsystem system
	
	# encrypt swap with a random key
	#cryptsetup open --type plain --key-file /dev/urandom /dev/disk/by-partlabel/cryptswap swap
}

# make sure efivars are available
modprobe efivars

# clear the target installation drive
sgdisk --zap-all "$DRIVE"

# create new partitions
# use labels to make everything much simpler going forward
sgdisk --clear \
       --new=1:0:+550MiB --typecode=1:ef00 --change-name=1:EFI \
       --new=2:0:+8GiB   --typecode=2:8200 --change-name=2:"$SWAP_LABEL" \
       --new=3:0:0       --typecode=3:8300 --change-name=3:"$SYS_LABEL" \
         "$DRIVE"

sleep 3

# format the boot partition as fat32
mkfs.fat -F32 -n EFI /dev/disk/by-partlabel/EFI

if [ ! -z $ENCRYPT ]
then
	echo "Configuring encryption..."
	setup_encryption
fi

# create the swap 
mkswap -L swap /dev/disk/by-partlabel/swap
#if [ -z $ENCRYPT ]
#then
#    mkswap -L swap /dev/disk/by-partlabel/swap
#else
#    mkswap -L swap /dev/disk/by-label/swap
#fi

sleep 3

# turn on swap
swapon /dev/disk/by-partlabel/swap
#if [ -z $ENCRYPT ]
#then
#    swapon /dev/disk/by-partlabel/swap
##else
##    swapon /dev/disk/by-label/swap
#fi

# format the root partition as btrfs
mkfs.btrfs --force --label system /dev/disk/by-partlabel/system

# mount options
# probably dont need plain o but meh
o=defaults
o_btrfs=$o,compress=lzo,ssd,noatime

# Stole subvol layout from Suse
# https://en.opensuse.org/SDB:BTRFS

# mount the root btrfs fs and create subvols
mount -t btrfs LABEL=system /mnt
btrfs subvolume create /mnt/root
btrfs subvolume create /mnt/home
btrfs subvolume create /mnt/opt
btrfs subvolume create /mnt/root_home
btrfs subvolume create /mnt/srv
btrfs subvolume create /mnt/tmp
btrfs subvolume create /mnt/usr_local
btrfs subvolume create /mnt/var

# unmount the btrfs fs
umount  /mnt

# mount the root subvol and create the needed sub directories
mount -t btrfs -o subvol=root,$o_btrfs LABEL=system /mnt
mkdir -p /mnt/{boot,home,opt,root,srv,tmp,usr,var}
mkdir /mnt/usr/local
mkdir /mnt/boot/efi

# mount the subdirectories
mount -t btrfs -o subvol=home,$o_btrfs LABEL=system /mnt/home
mount -t btrfs -o subvol=opt,$o_btrfs LABEL=system /mnt/opt
mount -t btrfs -o subvol=root_home,$o_btrfs LABEL=system /mnt/root
mount -t btrfs -o subvol=srv,$o_btrfs LABEL=system /mnt/srv
mount -t btrfs -o subvol=tmp,$o_btrfs LABEL=system /mnt/tmp
mount -t btrfs -o subvol=usr_local,$o_btrfs LABEL=system /mnt/usr/local
mount -t btrfs -o subvol=var,$o_btrfs LABEL=system /mnt/var
mount -t vfat LABEL=EFI /mnt/boot/efi

# disable COW on var
echo "Disabling COW on /var"
chattr +C /mnt/var

# run alpine-setup
setup-disk -m sys /mnt

# find the fastest repos
setup-apkrepos -c -f

# place the repos on the system
cp /etc/apk/repositories /mnt/etc/apk/repositories 

# add repos
#cat >> /mnt/etc/apk/repositories << EOF; $(echo)
#@testing_main http://dl-cdn.alpinelinux.org/alpine/edge/main
#@testing_comm http://dl-cdn.alpinelinux.org/alpine/edge/community
#@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing
#
#EOF

# bind mount /dev and /sys for installation
mount -t proc /proc /mnt/proc
mount --rbind /dev /mnt/dev
mount --make-rslave /mnt/dev
mount --rbind /sys /mnt/sys

# install bootloader pkgs in the new install
cat << EOF | chroot /mnt
apk add grub grub-efi efibootmgr
apk del syslinux
EOF

# run alpine setup steps
cat << EOF | chroot /mnt
setup-timezone -i America/Boise
setup-keymap us us
EOF

# add cryptsetup to mkinit features and setup key
if [ ! -z $ENCRYPT ]
then
    features=$(grep features /mnt/etc/mkinitfs/mkinitfs.conf | cut -d "=" -f 2 | tr -d '"')
    echo "features=\"${features} cryptsetup\"" > /mnt/etc/mkinitfs/mkinitfs.conf
    mkinitfs -c /mnt/etc/mkinitfs/mkinitfs.conf -b /mnt/ "$(ls /mnt/lib/modules/)"

    touch /mnt/crypto_keyfile.bin
    chmod 600 /mnt/crypto_keyfile.bin
    dd bs=512 count=4 if=/dev/urandom of=/mnt/crypto_keyfile.bin
    cryptsetup luksAddKey /dev/disk/by-partlabel/system /mnt/crypto_keyfile.bin
fi

# configure grub for encrypted boot
if [ ! -z $ENCRYPT ]
then
    uuid=$(blkid -s UUID -o value "$DRIVE")
    kernel_opts=$(grep GRUB_CMDLINE_LINUX_DEFAULT /mnt/etc/default/grub | cut -d "=" -f 2 | tr -d '"')
    sed -i 's/^GRUB_CMDLINE_LINUX_DEFAULT.*/GRUB_CMDLINE_LINUX_DEFAULT="${kernel_opts} cryptroot=${uuid} cryptdm=system cryptkey"/' /mnt/etc/default/grub
    sed -i 's/^GRUB_PRELOAD_MODULES.*/GRUB_PRELOAD_MODULES="luks cryptodisk part_gpt lvm"/' /mnt/etc/default/grub
fi

# install bootloader
cat << EOF | chroot /mnt
grub-install --target=x86_64-efi --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg
EOF

# setup snapper
#cat << EOF | chroot /mnt
#apk update
#apk add snapper
#snapper -c root create-config /
#snapper -c home create-config /home
#EOF

umount -l /mnt/dev
umount -l /mnt/proc
umount -l /mnt/sys
umount /mnt/boot/efi
swapoff /dev/disk/by-label/swap
umount /mnt/home
umount /mnt/opt
umount /mnt/root
umount /mnt/srv
umount /mnt/tmp
umount /mnt/usr/local
umount /mnt/var
umount /mnt
